
import express from 'express';
import cors from 'cors';
import { messageController } from './controller/message-controller';
import { personController } from './controller/person-controller';
import { dogController } from './controller/dog-controller';

export const server = express();

server.use(express.json());
server.use(cors());

server.use('/api/message', messageController)
server.use('/api/person', personController)
server.use('/api/dog', dogController)