import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Person } from "./Person";



@Entity()
export class Dog {
    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    name:string;
    @Column()
    breed:string;
    @Column({type: 'date'})
    birthdate:Date;
    @ManyToOne(() => Person, person => person.dogs, {onDelete:'SET NULL'}) //ici j'ai rajouté un ptit ON DELETE SET NULL pour faire qu'on puisse supprimer une personne qui a des chiens
    person:Person

}