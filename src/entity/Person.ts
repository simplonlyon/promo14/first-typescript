import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Dog } from "./Dog";

@Entity()
export class Person {
    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    name:string;
    @Column()
    age:number;

    @OneToMany(() => Dog, dog => dog.person)
    dogs:Dog[];
}
