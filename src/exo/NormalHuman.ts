


export class NormalHuman {

    /**
     * Manière alternative de déclarer des propriétés dans une classe
     * en créant directement un constructeur qui assigne les valeurs
     * aux propriétés.
     */
    constructor(
        public name:string,
        private age:number
    ) {}

    greet(person:NormalHuman):string {
        return `Hello ${person.name}, I am ${this.name}, ${this.age} years old !`;
    }

   
}


