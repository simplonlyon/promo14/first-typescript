import { Request, Router } from "express";

export const messageController = Router();

interface Message {
    sender:string;
    content:string;
    date?:Date|string;
}

export const messages:Message[] = [];
messages.push({
    content: 'test',
    sender: 'test',
    date: new Date('2021-01-04')
})

messageController.get('/', (req,res) => {
    res.json(messages);
});

/**
 * On peut typer le body de la request en indiquant un type en troisième
 * position de la Request dans les arguments, mais c'est au final un truc
 * qu'on fait pas très souvent et donc pas très utile
 */
messageController.post('/', (req:Request<{},{}, Message>, res) => {

    req.body.date = new Date();
    messages.push(req.body);
    res.status(201).json(req.body);
});
