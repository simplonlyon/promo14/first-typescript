import { Router } from "express";
import { getRepository } from "typeorm";
import { Person } from "../entity/Person";



export const personController = Router();

personController.get('/', async (req,res) => {
    try{
        

        const persons = await getRepository(Person).find();
        res.json(persons);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }

});

personController.post('/', async (req,res) => {
    try{
        let person = new Person();
        Object.assign(person, req.body);
        
        await getRepository(Person).save(person);
        
        res.status(201).json(person);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }

});


personController.get('/:id', async (req, res) => {
    try{
        const person = await getRepository(Person).findOne(req.params.id);
        if(!person) {
            res.status(404).end();
            return;
        }
        res.json(person);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});

personController.patch('/:id', async (req, res) => {
    try{
        const repo =  getRepository(Person);
        const person = await repo.findOne(req.params.id);
        if(!person) {
            res.status(404).end();
            return;
        } 
        Object.assign(person, req.body);
        repo.save(person);

        res.json(person);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})


personController.delete('/:id', async (req, res) => {
    try{
        const repo =  getRepository(Person);
        const result = await repo.delete(req.params.id);
        if(result.affected !== 1) {
            res.status(404).end();
            return;
        } 
        

        res.status(204).end();
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})