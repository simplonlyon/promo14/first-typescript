import { Router } from "express";
import { getRepository } from "typeorm";
import { Dog } from "../entity/Dog";


export const dogController = Router();


dogController.get('/', async (req,res) => {

    try {
        
        const dogs = await getRepository(Dog).find({relations: ['person']});

        res.json(dogs);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})



dogController.post('/', async (req,res) => {
    try{
        let dog = new Dog();
        Object.assign(dog, req.body);
        
        await getRepository(Dog).save(dog);
        
        res.status(201).json(dog);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }

});


dogController.get('/:id', async (req, res) => {
    try{
        const dog = await getRepository(Dog).findOne(req.params.id, {relations: ['person']});
        if(!dog) {
            res.status(404).end();
            return;
        }
        res.json(dog);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});

dogController.patch('/:id', async (req, res) => {
    try{
        const repo =  getRepository(Dog);
        const dog = await repo.findOne(req.params.id);
        if(!dog) {
            res.status(404).end();
            return;
        } 
        Object.assign(dog, req.body);
        repo.save(dog);

        res.json(dog);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})


dogController.delete('/:id', async (req, res) => {
    try{
        const repo =  getRepository(Dog);
        const result = await repo.delete(req.params.id);
        if(result.affected !== 1) {
            res.status(404).end();
            return;
        } 
        

        res.status(204).end();
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})