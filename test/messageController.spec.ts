
import request from 'supertest';
import { messages } from '../src/controller/message-controller';
import { server } from '../src/server';

describe('Message Controller', () => {

    beforeEach(() => {
        messages.length = 0;
    })
    
    it('should return all messages',  async () => {
        messages.push({
            content: 'test',
            sender: 'test',
            date:new Date()
        });
        const response = await request(server)
        .get('/api/message')
        .expect(200);

        expect(response.body).toContainEqual({
            content: expect.any(String),
            sender: expect.any(String),
            date: expect.any(String)
        });

    });

    it('should add a new message', async () => {

        const response = await request(server)
        .post('/api/message')
        .send({
            content: 'Test',
            sender:'Test'
        })
        .expect(201);

        expect(response.body).toEqual({
            content: expect.any(String),
            sender: expect.any(String),
            date: expect.any(String)
        });
        expect(messages.length).toBe(1);
    })

})