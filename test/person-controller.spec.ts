import request from 'supertest';
import { getManager } from 'typeorm';
import { Person } from '../src/entity/Person';
import { server } from '../src/server';
import { setUpTestDatabase } from './setUp';

describe('Person controller', () => {

    setUpTestDatabase();

    it('should return the persons on get', async () => {
        const response = await request(server)
        .get('/api/person')
        .expect(200);

        expect(response.body).toContainEqual({
            id: expect.any(Number),
            name: expect.any(String),
            age: expect.any(Number)
        });
    })

    it('should add a new person on post', async () => {
        const response = await request(server)
        .post('/api/person')
        .send({
            name: 'fromtest',
            age: 40
        })
        .expect(201);

        expect(response.body).toEqual({
            id: expect.any(Number),
            name: expect.any(String),
            age: expect.any(Number)
        });
        //Compter qu'on a bien une personne de plus en bdd après le post
        //pas obligé, mais pourquoi pas
        expect(await getManager().count(Person)).toBe(4);
    })

    it('should return a person on get with id', async () => {
        const response = await request(server)
        .get('/api/person/1')
        .expect(200);

        expect(response.body).toEqual({
            id: 1,
            name: expect.any(String),
            age: expect.any(Number)
        });
    })

    it('should return a 404 on unexisting person on GET/PATCH/DELETE', async () => {
        await request(server)
        .get('/api/person/100')
        .expect(404);
        await request(server)
        .patch('/api/person/100')
        .expect(404);
        await request(server)
        .delete('/api/person/100')
        .expect(404);

    });

    it('should remove a person on delete with id', async () => {
        await request(server)
        .delete('/api/person/1')
        .expect(204);

        expect(await getManager().count(Person)).toBe(2);

    })

    it('should modify a person on patch', async () => {
        const response = await request(server)
        .patch('/api/person/1')
        .send({
            name: 'fromtest'
        })
        .expect(200);

        expect(response.body).toEqual({
            id: 1,
            name: 'fromtest',
            age: 2
        });
    })
})
