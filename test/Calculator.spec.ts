import { Calculator } from "../src/exo/Calculator"

describe('Calculator class', () => {
    let calculator:Calculator;
    beforeEach(() => {
        calculator = new Calculator();
    });

    it('should multiply two values', () => {
        const result = calculator.calculate(10, 2, '*');

        expect(result).toBe(20);
    });
    it('should add two values', () => {
        const result = calculator.calculate(10, 2, '+');

        expect(result).toBe(12);
    });
    it('should throw error on wrong operator', () => {
        expect(() => calculator.calculate(10, 2, 'wrong')).toThrowError();
    });
})