import request from 'supertest';
import { getManager } from 'typeorm';
import { Dog } from '../src/entity/Dog';
import { server } from '../src/server';
import { setUpTestDatabase } from './setUp';

describe('Dog controller', () => {

    setUpTestDatabase();

    it('should return the dogs on get', async () => {
        const response = await request(server)
        .get('/api/dog')
        .expect(200);

        expect(response.body).toContainEqual({
            id: expect.any(Number),
            name: expect.any(String),
            birthdate: expect.any(String),
            breed: expect.any(String),
            person: expect.anything()
        });
    })

    it('should add a new dog on post', async () => {
        const response = await request(server)
        .post('/api/dog')
        .send({
            name: 'fromtest',
            birthdate: '2021-08-03',
            breed: 'breedtest',
            person: 1
        })
        .expect(201);

        expect(response.body).toEqual({
            id: expect.any(Number),
            name: expect.any(String),
            birthdate: expect.any(String),
            breed: expect.any(String),
            person:1
            
        });
        //Compter qu'on a bien une dogne de plus en bdd après le post
        //pas obligé, mais pourquoi pas
        expect(await getManager().count(Dog)).toBe(4);
    })

    it('should return a dog on get with id', async () => {
        const response = await request(server)
        .get('/api/dog/1')
        .expect(200);

        expect(response.body).toEqual({
            id: expect.any(Number),
            name: expect.any(String),
            birthdate: expect.any(String),
            breed: expect.any(String),
            person: expect.anything()
        });
    })

    it('should return a 404 on unexisting dog on GET/PATCH/DELETE', async () => {
        await request(server)
        .get('/api/dog/100')
        .expect(404);
        await request(server)
        .patch('/api/dog/100')
        .expect(404);
        await request(server)
        .delete('/api/dog/100')
        .expect(404);

    });

    it('should remove a dog on delete with id', async () => {
        await request(server)
        .delete('/api/dog/1')
        .expect(204);

        expect(await getManager().count(Dog)).toBe(2);

    })

    it('should modify a dog on patch', async () => {
        const response = await request(server)
        .patch('/api/dog/1')
        .send({
            name: 'fromtest'
        })
        .expect(200);

        expect(response.body).toEqual({
            id: 1,
            name: 'fromtest',
            birthdate: expect.any(String),
            breed: expect.any(String)
        });
    })
})
