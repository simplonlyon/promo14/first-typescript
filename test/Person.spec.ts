import { NormalHuman } from "../src/exo/NormalHuman";

describe('Person class', () => {

    it('should greet other person', () => {


        let person1 = new NormalHuman('test1', 1);
        let person2 = new NormalHuman('test2', 2);

        expect(person1.greet(person2)).toMatch('test1')
        expect(person1.greet(person2)).toMatch('test2')
    })
})